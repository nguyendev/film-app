import axios from "axios";
import { API_URL } from "./constant";

const instance = axios.create({
  baseURL: API_URL,
  params: {
    api_key: '8afd226979d44b2a273a29479239b565',
  },
});

export default instance;