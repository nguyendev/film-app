import { IMAGE_URL } from "./constant";

export const resizeImage = (
  imageUrl: string,
  width: string = "original"
): string => {
    return `${IMAGE_URL}/${width}${imageUrl}`
};
