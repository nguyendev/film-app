import { useQuery } from "@tanstack/react-query";
import { FC, useState } from "react";
import Header from "../../components/common/Header/Header";
import SideBar from "../../components/common/Sidebar/SideBar";
import MainHomeFilm from "../../components/Home/MainHomeFilm";
import { getHomeMovies, getMovieBannerInfo } from "../../services/home.service";
import { HomeFilms, Item } from "../../shared/types/film.interface";
import { useAppSelector } from "../../store/hooks";
import "./home.styles.scss";

function Home() {
  const currentUser = useAppSelector((state) => state.auth.user);
  const {
    data: dataMovie,
    isLoading: isLoadingMovie,
    isError: isErrorMovie,
    error: errorMovie,
  } = useQuery<HomeFilms, Error>(["home-movies"], getHomeMovies);

  const {
    data: dataMovieDetail,
    isLoading: isLoadingMovieDetail,
    isError: isErrorMovieDetail,
    error: errorMovieDetail,
  } = useQuery<any, Error>(
    ["detailMovies", dataMovie?.Trending],
    () => getMovieBannerInfo(dataMovie?.Trending as Item[]),
    { enabled: !!dataMovie?.Trending }
  );

  return (
    <>
      <div className="home-container">
        <div>
          <MainHomeFilm
            dataDetail={dataMovieDetail}
            data={dataMovie}
            isLoadingBanner={isLoadingMovieDetail}
            isLoadingSection={isLoadingMovie}
          />
        </div>
      </div>
    </>
  );
}

export default Home;
