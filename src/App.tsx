import { useEffect, useState } from "react";
import { Route, Routes } from "react-router-dom";
import "./App.css";
import Header from "./components/common/Header/Header";
import Movie from "./components/Movie/Movie";
import Home from "./pages/home/home";

function App() {
  const [isAuth, setIsAuth] = useState<boolean>();
  useEffect(() => {}, []);
  return (
    <>
      <Header />
      <Routes>
        <Route index element={<Home />} />
        <Route path="movie/:id" element={<Movie />} />
      </Routes>
    </>
  );
}

export default App;
