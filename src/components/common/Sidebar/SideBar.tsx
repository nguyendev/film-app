import { FC, useEffect, useState } from "react";
import "./sidebar.styles.scss";

function SideBar() {
  useEffect(() => {
  });
  return (
    <div className="contain-sidebar">
      <div className="logo">
        <img
          src="https://www.themoviedb.org/assets/2/v4/logos/v2/blue_square_1-5bdc75aaebeb75dc7ae79426ddd9be3b2be1e342510f8202baf6bffa71d7f5c4.svg"
          alt=""
        />
      </div>
      <div className="menu mt-6">
        <h1>Menu</h1>
        <ul>
          <li>
            <p>Home</p>
          </li>
          <li>
            <p>Explore</p>
          </li>
          <li>
            <p>Search</p>
          </li>
        </ul>
      </div>
      <div className="menu mt-6">
        <h1>Personal</h1>
        <ul>
          <li>
            <p>Bookmarked</p>
          </li>
          <li>
            <p>History</p>
          </li>
        </ul>
      </div>
    </div>
  );
}

export default SideBar;
