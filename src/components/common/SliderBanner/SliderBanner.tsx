import { FC, useEffect, useState } from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import { Item } from "../../../shared/types/film.interface";
import { LazyLoadImage } from "react-lazy-load-image-component";
import { resizeImage } from "../../../shared/utils";
import { Link } from "react-router-dom";
import SwiperCore, {
  Navigation,
  Pagination,
  Scrollbar,
  Zoom,
  Autoplay,
} from "swiper";
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "./SliderBanner.style.scss";

interface SliderBannerProps {
  films: Item[];
  dataDetail: {
    genre: { id: number; name: string }[];
    translation: string[];
  }[];
  isLoadingSection: boolean;
}

const SliderBanner: FC<SliderBannerProps> = ({
  films,
  dataDetail,
  isLoadingSection,
}) => {
  if (isLoadingSection) {
    return <>Loading</>;
  }
  return (
    <div className="wrap-banner mt-6">
      <Swiper
        modules={[Navigation, Autoplay]}
        navigation
        autoplay={{ delay: 5000, disableOnInteraction: false }}
        slidesPerView={1}
        className="swiper"
      >
        {(films as Item[]).map((film, index) => (
          <SwiperSlide className="swiper-slide" key={film.id}>
            <Link
              to={
                film.media_type === "movie"
                  ? `/movie/${film.id}`
                  : `/tv/${film.id}`
              }
              className="group"
            >
              <LazyLoadImage
                src={resizeImage(film.backdrop_path, "w1280")}
                alt="Backdrop image"
                effect="blur"
                className="img-banner"
              />
              <button className="btn">
                <span className="play"></span>
              </button>
              <div className="wrap-info-film">
                <div className="info-film">
                  <h1 className="title">{film.name || film.title}</h1>
                  <h3 className="sub-title mt-10">
                    {dataDetail[index].translation[0] ||
                      dataDetail[index].translation[1] ||
                      dataDetail[index].translation[2] ||
                      dataDetail[index].translation[3] ||
                      dataDetail[index].translation[4] ||
                      dataDetail[index].translation[5]}
                  </h3>
                  <p className="mt-6 text-note">
                    {film.release_date && `Release date: ${film.release_date}`}
                    {film.first_air_date &&
                      `First air date: ${film.first_air_date}`}
                  </p>
                  <div className="contain-genres">
                    {dataDetail[index].genre.map((item) => (
                      <div key={item.id} className="item-genre">
                        <p className="text-note m-0">{item.name}</p>
                      </div>
                    ))}
                  </div>
                  <p className="text-note mt-8">{film.overview}</p>
                </div>
              </div>
              <div className="contain-rating-point">
                <p className="m-0 me-4">{film.vote_average.toFixed(2)}</p>
                <svg
                  stroke="currentColor"
                  fill="currentColor"
                  strokeWidth="0"
                  viewBox="0 0 1024 1024"
                  height="15"
                  width="15"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path d="M908.1 353.1l-253.9-36.9L540.7 86.1c-3.1-6.3-8.2-11.4-14.5-14.5-15.8-7.8-35-1.3-42.9 14.5L369.8 316.2l-253.9 36.9c-7 1-13.4 4.3-18.3 9.3a32.05 32.05 0 0 0 .6 45.3l183.7 179.1-43.4 252.9a31.95 31.95 0 0 0 46.4 33.7L512 754l227.1 119.4c6.2 3.3 13.4 4.4 20.3 3.2 17.4-3 29.1-19.5 26.1-36.9l-43.4-252.9 183.7-179.1c5-4.9 8.3-11.3 9.3-18.3 2.7-17.5-9.5-33.7-27-36.3z"></path>
                </svg>
              </div>
            </Link>
          </SwiperSlide>
        ))}
      </Swiper>
    </div>
  );
};

export default SliderBanner;
