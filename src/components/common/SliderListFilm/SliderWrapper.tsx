import React, { FC } from "react";
import "./SliderListStyle/SliderWrapper.scss";

const SliderWrapper: FC<any> = ({ children }) => (
  <div className="slider-wrapper">{children}</div>
);

export default SliderWrapper;
