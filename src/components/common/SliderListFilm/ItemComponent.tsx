import React, { FC } from "react";
import cx from "classnames";
import SliderContext from "./context";
// import Mark from './Mark'
import "./SliderListStyle/Item.scss";
import { resizeImage } from "../../../shared/utils";
import { Item } from "../../../shared/types/film.interface";

const ItemComponent: FC<any> = ({ movie }) => (
  <SliderContext.Consumer>
    {({ onSelectSlide, currentSlide, elementRef }) => {
      const isActive = currentSlide && currentSlide.id === movie.id;
      console.log(movie);

      return (
        <div
          ref={elementRef}
          className={cx("item", {
            "item--open": isActive,
          })}
        >
          <img src={resizeImage(movie.backdrop_path, "w1280")} alt="" />
          <div className="info-film">
            <p className="title">{movie.name || movie.title}</p>
          </div>
          <h3 className="btn-play-item">
            <svg
              className="MuiSvgIcon-root"
              focusable="false"
              viewBox="0 0 24 24"
              aria-hidden="true"
            >
              <path d="M10 16.5l6-4.5-6-4.5v9zM12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8z"></path>
            </svg>
          </h3>
        </div>
      );
    }}
  </SliderContext.Consumer>
);

export default ItemComponent;
