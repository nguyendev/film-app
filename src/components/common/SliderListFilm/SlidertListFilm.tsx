import React, { useState, FC } from "react";
import cx from "classnames";
import useSizeElement from "../../../customHook/useSizeElement";
import useSliding from "../../../customHook/useSliding";
import "./SliderListStyle/SliderListFilm.scss";
import SliderContext from "./context";
import SliderWrapper from "./SliderWrapper";
import SlideButton from "./SlideButton";

const SlideListFIlm: FC<any> = ({ children, activeSlide }) => {
  const [currentSlide, setCurrentSlide] = useState(activeSlide);
  const { width, elementRef } = useSizeElement();
  const handleSelect = (movie: any) => {
    setCurrentSlide(movie);
  };

  const handleClose = () => {
    setCurrentSlide(null);
  };

  const { handlePrev, handleNext, slideProps, containerRef, hasNext, hasPrev } =
    useSliding(width, React.Children.count(children));
  console.log(hasNext);
    
  const contextValue = {
    onSelectSlide: handleSelect,
    onCloseSlide: handleClose,
    elementRef,
    currentSlide,
  };
  return (
    <>
      <SliderContext.Provider value={contextValue}>
        <SliderWrapper>
          <div
            className={cx("slider", { "slider--open": currentSlide != null })}
          >
            <div
              ref={containerRef}
              className="slider__container"
              {...slideProps}
            >
              {children}
            </div>
          </div>
          {hasPrev && <SlideButton onClick={handlePrev} type="prev" />}
          {hasNext && <SlideButton onClick={handleNext} type="next" />}
        </SliderWrapper>
      </SliderContext.Provider>
    </>
  );
};

export default SlideListFIlm;