import { useState, useRef } from "react";
import { useLocation, Link } from "react-router-dom";
import "./header.style.scss";

function Header() {
  const [isOpenSearch, setIsOpenSearch] = useState<boolean>();
  const search = useRef<HTMLInputElement>(null);
  const location = useLocation();

  const openSearch = (): void => {
    setIsOpenSearch(true);
    setTimeout(() => {
      search.current?.focus();
    }, 0);
  };
  const closeSearchBox = (): void => {
    setIsOpenSearch(false);
  };
  return (
    <div className="wrap-header">
      <div className="pinning-header">
        <div className="main-header">
          <img
            className="logo"
            src="https://www.themoviedb.org/assets/2/v4/logos/v2/blue_square_1-5bdc75aaebeb75dc7ae79426ddd9be3b2be1e342510f8202baf6bffa71d7f5c4.svg"
          />
          <ul>
            <li>
              <Link to="/">Home</Link>{" "}
            </li>
            <li>
              <Link to="/">TV shows</Link>
            </li>
            <li>
              <Link to="/">Movies</Link>
            </li>
            <li>
              <Link to="/">New popular</Link>
            </li>
            <li>
              <Link to="/">My list</Link>
            </li>
          </ul>
          <div className="secondary-navigate">
            <div className="search-box">
              <div className={"search-input " + (isOpenSearch ? "active" : "")}>
                <input
                  ref={search}
                  className={isOpenSearch ? "active" : ""}
                  onBlur={closeSearchBox}
                  placeholder="Titles, people, genres"
                  type="text"
                />
              </div>
              <button
                className={"btn-search " + (isOpenSearch ? "active" : "")}
                onClick={openSearch}
              >
                <svg
                  width="24"
                  height="24"
                  viewBox="0 0 24 24"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                  className="search-icon"
                >
                  <path
                    fillRule="evenodd"
                    clipRule="evenodd"
                    d="M13 11C13 13.7614 10.7614 16 8 16C5.23858 16 3 13.7614 3 11C3 8.23858 5.23858 6 8 6C10.7614 6 13 8.23858 13 11ZM14.0425 16.2431C12.5758 17.932 10.4126 19 8 19C3.58172 19 0 15.4183 0 11C0 6.58172 3.58172 3 8 3C12.4183 3 16 6.58172 16 11C16 11.9287 15.8417 12.8205 15.5507 13.6497L24.2533 18.7028L22.7468 21.2972L14.0425 16.2431Z"
                    fill="currentColor"
                  ></path>
                </svg>
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Header;
