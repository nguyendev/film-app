import React, { FC, useEffect, useState } from "react";
import { HomeFilms, Item } from "../../shared/types/film.interface";
import SliderBanner from "../common/SliderBanner/SliderBanner";
import ItemComponent from "../common/SliderListFilm/ItemComponent";
import SlideListFIlm from "../common/SliderListFilm/SlidertListFilm";
import "./MainHomeFilm.style.scss";

interface MainHomeFilmsProps {
  data: HomeFilms | undefined;
  dataDetail: any;
  isLoadingBanner: boolean;
  isLoadingSection: boolean;
}

const MainHomeFilm: FC<MainHomeFilmsProps> = ({
  data,
  dataDetail,
  isLoadingBanner,
  isLoadingSection,
}) => {
  
  return (
    <div>
      {!isLoadingSection && data ? (
        <>
          <SliderBanner
            films={data?.Trending as Item[]}
            isLoadingSection={isLoadingBanner}
            dataDetail={dataDetail}
          />
          <div className="wrap-type-film">
            <div className="wrap-container">
              <h2 className="title-type mt-8">Hot</h2>
              <SlideListFIlm>
                {data && data?.Hot.map((movie) => (
                  <ItemComponent movie={movie} key={movie.id}>
                    item1
                  </ItemComponent>
                ))}
              </SlideListFIlm>
              <h2 className="title-type mt-8">Popular</h2>
              <SlideListFIlm>
                {data && data?.Popular.map((movie) => (
                  <ItemComponent movie={movie} key={movie.id}>
                    item1
                  </ItemComponent>
                ))}
              </SlideListFIlm>
              <h2 className="title-type mt-8">Top Rated</h2>
              <SlideListFIlm>
                {data && data['Top Rated'].map((movie) => (
                  <ItemComponent movie={movie} key={movie.id}>
                    item1
                  </ItemComponent>
                ))}
              </SlideListFIlm>
            </div>
          </div>
        </>
      ) : (
        "Loading"
      )}
    </div>
  );
};

export default MainHomeFilm;
